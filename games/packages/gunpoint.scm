;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages gunpoint)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages mono)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages game-development)
  #:use-module (games packages libpng)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle))

(define-public gunpoint
  (let* ((version "170914_1410971363")
         (file-name (string-append "GunpointLinux_" version ".tar.gz"))
         (binary "Gunpoint"))
    (package
      (name "gunpoint")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "1gg3y25d0xx37j1zaccnihiipww4xww92ism064yv3m408s6pscg"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `((,,binary
            ("gcc" "openal" "libx11" "mesa" "libpng" "libvorbis" "nvidia-cg-toolkit")))
         #:install-plan
         `(("." "share/gunpoint"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'clean-up-libs
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (share (string-append output "/share/gunpoint/")))
                 (for-each
                  (lambda (f)
                    (delete-file (string-append share f)))
                  '("libXrandr.so.2" "libogg.so.0" "libopenal.so.1"
                    "libvorbis.so.0" "libvorbisfile.so.3"
                    "libCg.so" "libCgGL.so"))
                 #t)))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/gunpoint"))
                      (real (string-append output "/share/gunpoint/" ,binary)))
                 (chmod real #o755)
                 (make-wrapper wrapper real)
                 (make-desktop-entry-file (string-append output "/share/applications/reus.desktop")
                                          #:name "Gunpoint"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/Gunpoint.bmp")
                                          #:comment "A stealth puzzle game that lets you rewire its levels to trick people"
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("gcc" ,gcc "lib")
         ("openal" ,openal)
         ("libx11" ,libx11)
         ("mesa" ,mesa)
         ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)
         ("libpng" ,libpng-1.2)
         ("libvorbis" ,libvorbis)
         ("libogg" ,libogg)))
      (home-page "https://www.gunpointgame.com/")
      (synopsis "Stealth puzzle game that lets you rewire its levels to trick people")
      (description "Gunpoint is set in the near future and sees players assume
the role of freelance spy Richard Conway, who is tasked with infiltrating
buildings to fulfil assignments from various clients.  To do so, the player must
avoid guards and bypass security features with the aid of a number of high-tech
gadgets, such as the Crosslink tool which is used to rewire electrical circuits.
Throughout the game, Conway seeks to clear his name in the murder of a
high-profile weapons manufacturer, and gradually uncovers a murder mystery in
his search for the real killer.")
      (license (undistributable "No URL")))))
